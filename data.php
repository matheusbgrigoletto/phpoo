<?php

define('CLASS_DIR', 'src' . PATH_SEPARATOR);
set_include_path(get_include_path() . PATH_SEPARATOR . CLASS_DIR);
spl_autoload_register();

$clientes = [];

for($i = 0; $i < 10; $i++) {
    $x = $i+1;

    if($x % 2 == 0) {
        $clientes[$i] = new \POO\Cliente\ClienteFisica();
    } else {
        $clientes[$i] = new \POO\Cliente\ClienteJuridica();
    }


    $clientes[$i]->setNome($x . 'o. Cliente');

    if($x==10) $x=0;

    $clientes[$i]->setEmail('cliente'.$x.'@email.com');
    $clientes[$i]->setEndereco('Rua Lorem Ipsum, 45'.$x);
    $clientes[$i]->setTelefone('(19) 345'.$x.'-32'.$x.'7');

    if($clientes[$i] instanceof \POO\Cliente\ClienteFisica) {
        $clientes[$i]->setCpf(str_repeat($x, 11));
    } else {
        $clientes[$i]->setCnpj(str_repeat($x, 13));
    }

    if(mt_rand(0, 10) < 5) {
        $clientes[$i]->setEnderecoCobranca('Av. Dolor Cit Amet, 81'.$x);
    } else {
        $clientes[$i]->setEnderecoCobranca('--');
    }

    $clientes[$i]->setGrau( mt_rand(1, 5) );
}