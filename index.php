<?php
require 'data.php';
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">

    <meta name="description" content="">
    <meta name="author" content="Matheus B. Grigoletto">

    <title>PHP OO</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PHP OO</a>
        </div>
    </div>
</nav>

<div class="container">

        <div class="col-sm-12 main">
            <h1 class="page-header">Clientes</h1>

            <div class="table-responsive">
                <table class="table table-striped tablesorter">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Tipo</th>
                        <th>CPF/CNPJ</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Endere&ccedil;o</th>
                        <th>Endere&ccedil;o de Cobran&ccedil;a</th>
                        <th>Estrelas</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($clientes as $i => $row)
                    {
                        echo '<tr>'.
                            '<td>'.$i.'</td>'.
                            '<td>'.$row->getNome().'</td>';

                        if($row instanceof \POO\Cliente\ClienteFisica) {
                            echo '<td>Pessoa f&iacute;sica</td>' .
                                '<td>'.$row->getCpf().'</td>';
                        } else {
                            echo '<td>Pessoa jur&iacute;dica</td>' .
                                '<td>'.$row->getCnpj().'</td>';
                        }

                        echo '<td>'.$row->getEmail().'</td>'.
                            '<td>'.$row->getTelefone().'</td>'.
                            '<td>'.$row->getEndereco().'</td>'.
                            '<td>'.$row->getEnderecoCobranca().'</td>'.
                            '<td><span class="hidden">'.$row->getGrau().'</span>'.str_repeat('<img src="assets/images/star.png" alt="*">', $row->getGrau()).'</td>'.
                        '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/jquery.tablesorter.min.js"></script>
<script src="assets/js/script.js"></script>
</body>
</html>
