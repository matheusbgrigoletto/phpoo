<?php

namespace POO\Cliente;

interface InterfaceEnderecoCobranca {

    public function setEnderecoCobranca($enderecoCobranca);
    public function getEnderecoCobranca();

}