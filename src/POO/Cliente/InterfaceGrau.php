<?php

namespace POO\Cliente;

interface InterfaceGrau {

    public function setGrau($grau);
    public function getGrau();

}